-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 23-Nov-2020 às 12:14
-- Versão do servidor: 10.4.16-MariaDB
-- versão do PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `analise-credito`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE `endereco` (
  `id` bigint(20) NOT NULL,
  `bairro` varchar(50) DEFAULT NULL,
  `cidade` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `rua` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`id`, `bairro`, `cidade`, `complemento`, `estado`, `numero`, `rua`) VALUES
(1, 'oitizeiro', 'João Pessoa', 'casa', 'Paraiba', '111', 'pr jose severino de oliveira'),
(2, '23', 'João Pessoa', '23', 'PB', '23', 'pr jose severino de oliveira'),
(3, 'teste', 'João Pessoa', 'teste', 'PB', '12', 'teste'),
(4, 'cristo', 'João Pessoa', 'ap 300', 'PB', '50', 'nova'),
(5, 'Alto do mateus', 'João Pessoa', 'casa', 'PB', '333', 'Velha'),
(6, 'oit', 'João Pessoa', 'casa', 'PB', '111', 'alfaltada'),
(7, ' vq', 'João Pessoa', ' ', 'PB', ' Q', ' qWR'),
(8, 'DSV Sd ', 'João Pessoa', 'X zx', 'PB', 'S s ', 's s '),
(9, 'WF', 'João Pessoa', ' Q', 'PB', 'W f', ' we'),
(10, 's aehr', 'João Pessoa', ' sgd', 'PB', ' aeg ', ' W '),
(11, 'João agripino', 'João Pessoa', 'casa', 'PB', '111', 'nova'),
(12, 'jape', 'João Pessoa', 'fwqw', 'PB', '4646', 'wew'),
(13, 'Alto da boa vista', 'João Pessoa', 'Casa', 'PB', '1234', 'Jose Antonio'),
(14, 'cristo', 'João Pessoa', 'casa', 'PB', '4555', 'Nova');

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE `perfil` (
  `id` bigint(20) NOT NULL,
  `cd_perfil` varchar(50) DEFAULT NULL,
  `descricao` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `cd_perfil`, `descricao`) VALUES
(1, 'ADMIN', 'administrador'),
(2, 'ANALIST', 'analista'),
(3, 'ATEND', 'atendente');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `id` bigint(20) NOT NULL,
  `cpf` varchar(50) DEFAULT NULL,
  `dt_nascimento` datetime DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `telefone` varchar(50) DEFAULT NULL,
  `id_endereco` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`id`, `cpf`, `dt_nascimento`, `email`, `nome`, `telefone`, `id_endereco`) VALUES
(1, '07585463430', '1988-10-23 12:01:07', 'andrefariasti@gmail.com', 'André Luis Farias', '83988631130', 1),
(2, '21278222006', '1988-10-23 12:01:07', 'fracarlaoc@gmail.com', 'Francelly carla farias', '83988631130', 1),
(3, '10321997417', '1988-10-23 12:01:07', 'teste@gmail.com', 'Liendson Doglas', '83988631130', 2),
(4, '00321997418', '1988-10-23 12:01:07', 'teste@gmail.com', 'Yasmin Isabel', '83988631130', 1),
(5, '00221997418', '1988-10-23 12:01:07', 'teste@gmail.com', 'Sara Helene', '83988631130', 2),
(7, '10321999417', '1969-12-31 23:47:41', 'andrefariasti@gmail.com', 'João Pedro', '83988631130', 2),
(8, '12131231212', '1998-06-09 21:00:00', 'andrefariasti@gmail.com', 'Paiva de Queiroz', '83988631130', 3),
(9, '08764839876', '1999-03-01 21:00:00', 'andrefariasti@gmail.com', 'Joaquim da silva', '83988631130', 4),
(10, '46320536004', '2020-11-07 21:00:00', 'andrefariasti@gmail.com', 'Claudio Antonio', '83988631130', 5),
(11, '12345678909', '2005-02-15 22:00:00', 'andrefariasti@gmail.com', 'Chico Tavares', '83988631130', 6),
(12, '45600618077', '2020-11-04 21:00:00', 'andrefariasti@gmail.com', 'Jose Amado', '83988631130', 7),
(13, '81401471099', '2020-11-04 21:00:00', 'andrefariasti@gmail.com', 'Anastacio Almeida', '83988631130', 8),
(14, '83514469040', '2020-11-03 21:00:00', 'andrefariasti@gmail.com', 'Arthur franca', '83988631130', 9),
(15, '94343792021', '2020-11-04 21:00:00', 'andrefariasti@gmail.com', 'Severino Bezerra', '83988631130', 10),
(16, '99999999999', '2020-11-16 21:00:00', 'andrefariasti@gmail.com', 'Maria jose', '83988631130', 11),
(17, '36673517042', '2020-11-04 21:00:00', 'andrefariasti@gmail.com', 'Jose Paiva', '83988631130', 12),
(18, '84423569086', '2020-07-14 21:00:00', 'andrefariasti@gmail.com', 'Junior de Andrade', '88790576665', 13),
(19, '49010696081', NULL, 'andrefariasti@gmail.com', 'Ana Paula Farias', '87654896068', 14);

-- --------------------------------------------------------

--
-- Estrutura da tabela `proposta`
--

CREATE TABLE `proposta` (
  `id` bigint(20) NOT NULL,
  `dh_proposta` datetime DEFAULT NULL,
  `situacao` int(11) DEFAULT NULL,
  `id_pessoa` bigint(20) DEFAULT NULL,
  `id_usuario` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `proposta`
--

INSERT INTO `proposta` (`id`, `dh_proposta`, `situacao`, `id_pessoa`, `id_usuario`) VALUES
(2, '2020-11-21 18:16:43', 0, 3, 2),
(3, '2020-11-21 18:16:43', 2, 4, 2),
(4, '2020-11-21 18:16:43', 2, 5, 2),
(5, '2020-11-22 14:33:12', 0, 7, 2),
(6, '2020-11-22 17:09:38', 2, 8, 2),
(7, '2020-11-22 17:12:43', 0, 9, 2),
(8, '2020-11-22 17:16:56', 2, 10, 2),
(9, '2020-11-22 19:36:16', 1, 11, 2),
(10, '2020-11-22 19:37:57', 2, 12, 2),
(11, '2020-11-22 19:39:37', 1, 13, 2),
(12, '2020-11-22 19:40:25', 1, 14, 2),
(13, '2020-11-22 19:45:59', 0, 15, 2),
(14, '2020-11-23 00:06:57', 1, 16, 2),
(15, '2020-11-23 02:18:38', 1, 17, 2),
(16, '2020-11-23 07:41:58', 1, 18, 2),
(17, '2020-11-23 07:43:57', 1, 19, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usr_perfil`
--

CREATE TABLE `usr_perfil` (
  `id` bigint(20) NOT NULL,
  `id_perfil` bigint(20) DEFAULT NULL,
  `id_usuario` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `usr_perfil`
--

INSERT INTO `usr_perfil` (`id`, `id_perfil`, `id_usuario`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` bigint(20) NOT NULL,
  `login` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `id_pessoa` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `login`, `senha`, `id_pessoa`) VALUES
(1, 'andre', '12345', 1),
(2, 'fran', '12345', 2);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKo2v72927b43j7ug0s8b97ymly` (`id_endereco`);

--
-- Índices para tabela `proposta`
--
ALTER TABLE `proposta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK42u72auqrq4j2uk806f7vikyg` (`id_pessoa`),
  ADD KEY `FKn8sb3pilmfjyg9jqap5bedpmc` (`id_usuario`);

--
-- Índices para tabela `usr_perfil`
--
ALTER TABLE `usr_perfil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKnhr2bjsbrxv1lyh5fwimtktjf` (`id_perfil`),
  ADD KEY `FKfogt10hlxy51vp80oabgy51hc` (`id_usuario`);

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKihpr6fsj9vxc6aqg9seaicql6` (`id_pessoa`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `endereco`
--
ALTER TABLE `endereco`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de tabela `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de tabela `proposta`
--
ALTER TABLE `proposta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de tabela `usr_perfil`
--
ALTER TABLE `usr_perfil`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `pessoa`
--
ALTER TABLE `pessoa`
  ADD CONSTRAINT `FKo2v72927b43j7ug0s8b97ymly` FOREIGN KEY (`id_endereco`) REFERENCES `endereco` (`id`);

--
-- Limitadores para a tabela `proposta`
--
ALTER TABLE `proposta`
  ADD CONSTRAINT `FK42u72auqrq4j2uk806f7vikyg` FOREIGN KEY (`id_pessoa`) REFERENCES `pessoa` (`id`),
  ADD CONSTRAINT `FKn8sb3pilmfjyg9jqap5bedpmc` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Limitadores para a tabela `usr_perfil`
--
ALTER TABLE `usr_perfil`
  ADD CONSTRAINT `FKfogt10hlxy51vp80oabgy51hc` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `FKnhr2bjsbrxv1lyh5fwimtktjf` FOREIGN KEY (`id_perfil`) REFERENCES `perfil` (`id`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `FKihpr6fsj9vxc6aqg9seaicql6` FOREIGN KEY (`id_pessoa`) REFERENCES `pessoa` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
