# Desafio-conductor

### Requisitos de sistema
    *banco mysql instalado
    *Node v10.3 em diante
    *Npm v6.4.1 em diante

### Instruções
 - Após o projeto ser clonado deverá ser aberto em uma ide de sua preferência, recomendo o IntelliJ. 
     
   ```
   https://gitlab.com/andrefariasti/desafio-conductor.git
 
 - Criar um banco de dados mysql no seu SGBD, execute o script abaixo:
 
   ```
   CREATE DATABASE analisecredito;

  -  Deverá ser importado o script de criação de tabelas e massa de dados  para teste. Caso esteja usando o phpMyAdmin, acesse seu o banco criado e no comando "Importar" deverá ser importado o arquivo que esta na pasta "script" dentro do projeto no caminha abaixo:

        ```
       \desafio-conductor\script\analise-credito.sql
   
### Rodando a aplicação

  - Na parte de back-end executar o comando 'RUN';
  - Abra um terminal e acesse a pasta 'camada-view' e rode os seguintes comandos:
 
    ```
    npm install
    ng serve
 
### Acessando a aplicação

  - No seu navegador acesse http://localhost:4200/;
  - o sistema possui dois usuário que são:
 
    ```
    Analista
    usuario: andre
    senha: 12345
    
    Atendente
    usuario: fran
    senha : 12345
 
### Sobre a aplicação

   - A aplicação possibilita a inclusão de dados para análise de crédito, onde ao logar na aplicação dependendo do seu perfil, será redirecionado para a tela de cadastro de proposta ou para tela de propostas pendentes.
   - O sistema verifica o seu perfil a cada tela, isso para saber se o usuário tem permissão para acessar a mesma, caso o usuário tente acessar via url.
   - A tela de consultar proposta é comum para ambos os perfis (Analista, atendente);   
  