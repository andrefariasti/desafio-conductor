package com.conductor.analisecredito.service;

import com.conductor.analisecredito.entidade.Endereco;
import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.entidade.Proposta;
import com.conductor.analisecredito.repository.PropostaRepository;
import com.conductor.analisecredito.util.SituacaoPropostaEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PropostaServiceImpl implements PropostaService {

    @Autowired
    private PropostaRepository repository;

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private EnderecoService enderecoService;

    @Override
    public Proposta salvar(Proposta proposta) throws Exception {

        Endereco endereco = enderecoService.salvar(proposta.getPessoa().getEndereco());
        proposta.getPessoa().setEndereco(endereco);

        Pessoa pessoa = pessoaService.salvar(proposta.getPessoa());
        proposta.setPessoa(pessoa);
        proposta.setDhProposta(new Date());
        proposta.setSituacao(SituacaoPropostaEnum.EM_ANALISE);
        proposta.setUsuarioAtendente(usuarioService.buscarUsuarioLogin("fran"));

        return repository.save(proposta);
    }

    @Override
    public Proposta rejeitarProposta(Long id) throws Exception {

        Optional<Proposta> proposta = repository.findById(id);
        proposta.ifPresent(value -> value.setSituacao(SituacaoPropostaEnum.REJEITADA));

        return repository.save(proposta.get());
    }

    @Override
    public Proposta aprovarProposta(Long id) throws Exception {

        Optional<Proposta> proposta = repository.findById(id);
        proposta.ifPresent(value -> value.setSituacao(SituacaoPropostaEnum.APROVADA));

        return repository.save(proposta.get());
    }

    @Override
    public Optional<Proposta> buscarPropostaId(Long id) throws Exception {
        return repository.findById(id);
    }

    @Override
    public List<Proposta> buscarProposta(String nome, String cpf) throws Exception {

        List<Pessoa> listaPessoa = new ArrayList<>();

        if (Objects.nonNull(cpf) && !cpf.isEmpty()) {
            Pessoa pessoa = pessoaService.buscarPessoaPorCPF(linparString(cpf));
            if (Objects.nonNull(pessoa)) {
                listaPessoa.add(pessoa);
            }
        } else if (Objects.nonNull(nome) && !nome.isEmpty()) {
            listaPessoa = pessoaService.buscarPessoasPorNome(nome);
        }

        return repository.findAllByPessoaIn(listaPessoa);
    }

    @Override
    public List<Proposta> buscarTodasPropostas() throws Exception {
        return repository.findBySituacao(SituacaoPropostaEnum.EM_ANALISE);
    }

    private String linparString(String string) {
        string = string.replace("-", "");
        string = string.replace(".", "");
        return string;
    }
}