package com.conductor.analisecredito.service;

import com.conductor.analisecredito.entidade.Endereco;
import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.repository.EnderecoRepository;
import com.conductor.analisecredito.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EnderecoServiceImpl implements EnderecoService{

    @Autowired
    private EnderecoRepository enderecoRepository;

    @Override
    public Endereco salvar(Endereco endereco) throws Exception {
        return enderecoRepository.saveAndFlush(endereco);
    }
}
