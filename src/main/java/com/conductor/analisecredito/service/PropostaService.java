package com.conductor.analisecredito.service;

import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.entidade.Proposta;

import java.util.List;
import java.util.Optional;

public interface PropostaService {

    Proposta salvar(Proposta proposta) throws Exception;

    Optional<Proposta> buscarPropostaId(Long id) throws Exception;

    List<Proposta> buscarProposta(String nome, String cpf) throws Exception;

    List<Proposta> buscarTodasPropostas() throws Exception;

    Proposta rejeitarProposta(Long id) throws Exception;

    Proposta aprovarProposta(Long id) throws Exception;

}
