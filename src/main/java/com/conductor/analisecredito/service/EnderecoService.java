package com.conductor.analisecredito.service;

import com.conductor.analisecredito.entidade.Endereco;
import com.conductor.analisecredito.entidade.Pessoa;

import java.util.Optional;

public interface EnderecoService {

    Endereco salvar(Endereco endereco) throws Exception;

}
