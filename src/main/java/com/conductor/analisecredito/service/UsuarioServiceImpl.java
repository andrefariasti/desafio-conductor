package com.conductor.analisecredito.service;

import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.entidade.Usuario;
import com.conductor.analisecredito.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService{

    @Autowired
    UsuarioRepository repository;

    @Override
    public Usuario salvar(Usuario usuario) throws Exception {
        return repository.save(usuario);
    }

    @Override
    public Optional<Usuario> buscarUsuarioId(Long id) throws Exception {
        return repository.findById(id);
    }

    @Override
    public Usuario buscarUsuarioLogin(String longin) throws Exception {
        return repository.findByLogin(longin);
    }
}
