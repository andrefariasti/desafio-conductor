package com.conductor.analisecredito.service;

import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.entidade.Usuario;

import java.util.Optional;

public interface UsuarioService {

    Usuario salvar(Usuario usuario) throws Exception;

    Optional<Usuario> buscarUsuarioId(Long id) throws  Exception;

    Usuario buscarUsuarioLogin(String login) throws Exception;
}
