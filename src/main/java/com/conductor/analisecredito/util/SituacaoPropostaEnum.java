package com.conductor.analisecredito.util;


import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum SituacaoPropostaEnum {

    APROVADA(0L, "Aprovado"),
    EM_ANALISE(1L, "Em Análise"),
    REJEITADA(2L, "Rejeitado");

    private Long id;
    private String descricao;

    SituacaoPropostaEnum(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public static SituacaoPropostaEnum valueOfTipo(Long id) {
        for (SituacaoPropostaEnum tp : SituacaoPropostaEnum.values()) {
            if (tp.id.equals(id)) {
                return tp;
            }
        }
        return null;
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
