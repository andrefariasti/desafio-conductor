package com.conductor.analisecredito.entidade;

import com.conductor.analisecredito.util.SituacaoPropostaEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.EnumDeserializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "proposta")
public class Proposta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "idPessoa")
    private Pessoa pessoa;

    @Column(nullable = true)
    private Date dhProposta;

    @Column(nullable = true)
    @Enumerated(EnumType.ORDINAL)
    private SituacaoPropostaEnum situacao;

    @OneToOne
    @JoinColumn(name = "idUsuario")
    private Usuario usuarioAtendente;
}
