package com.conductor.analisecredito.controller;

import com.conductor.analisecredito.entidade.Proposta;
import com.conductor.analisecredito.service.PropostaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping({"/proposta"})
public class PropostaController {

    @Autowired
    PropostaService propostaService;

    @GetMapping(path = {"/{id}"})
    public ResponseEntity findById(@PathVariable Long id) throws Exception {
        return propostaService.buscarPropostaId(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(path = {"/salvar"})
    public ResponseEntity<Proposta> findById(@RequestBody Proposta proposta) throws Exception {
        return ResponseEntity.ok().body(propostaService.salvar(proposta));
    }

    @GetMapping(path = {"/buscar"})
    public ResponseEntity<List<Proposta>> buscar() throws Exception {
        return ResponseEntity.ok().body(propostaService.buscarTodasPropostas());
    }

    @GetMapping(value = "/buscar/{id}")
    public ResponseEntity<Proposta> buscarPorId(@PathVariable Long id) throws Exception {
        return propostaService.buscarPropostaId(id)
            .map(record -> ResponseEntity.ok().body(record))
            .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/buscar-por-filtro")
    public ResponseEntity<List<Proposta>> buscarPorFiltro(@RequestParam(required = false) String nome,
                                                          @RequestParam(required = false) String cpf) throws Exception {
        return ResponseEntity.ok().body(propostaService.buscarProposta(nome, cpf));
    }

    @GetMapping(value = "/aprovar/{id}")
    public ResponseEntity<Proposta> analisar(@PathVariable Long id) throws Exception {

        Proposta proposta = propostaService.aprovarProposta(id);
        return ResponseEntity.ok().body(proposta);
    }

    @GetMapping(value = "/rejeitar/{id}")
    public ResponseEntity<Proposta> rejeitar(@PathVariable Long id) throws Exception {

        Proposta proposta = propostaService.rejeitarProposta(id);
        return ResponseEntity.ok().body(proposta);
    }

}
