package com.conductor.analisecredito.controller;

import com.conductor.analisecredito.entidade.Perfil;
import com.conductor.analisecredito.entidade.Usuario;
import com.conductor.analisecredito.entidade.dto.LoginDTO;
import com.conductor.analisecredito.repository.PerfilRepository;
import com.conductor.analisecredito.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
public class AutenticacaoController {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    PerfilRepository perfilRepository;

    @PostMapping(value = "/login")
    public ResponseEntity<LoginDTO> login(@RequestBody LoginDTO loginDTO) throws Exception {

        Usuario usuario = usuarioRepository.findByLogin(loginDTO.getLogin());

        LoginDTO usuarioLogado = null;

        if (Objects.isNull(usuario)) {
            return ResponseEntity.noContent().build();
        }

        if (usuario.getSenha().equals(loginDTO.getSenha())) {
            List<Perfil> listaPerfil = perfilRepository.buscarPerfisUsuario(usuario.getId());

            usuarioLogado =
                LoginDTO.builder()
                    .login(usuario.getLogin())
                    .senha(usuario.getSenha())
                    .perfil(listaPerfil)
                    .usuario(usuario).build();

            usuarioLogado.setPerfil(listaPerfil);
        }

        return ResponseEntity.ok().body(usuarioLogado);
    }

}
