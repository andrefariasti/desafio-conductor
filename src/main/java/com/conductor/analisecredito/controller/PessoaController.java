package com.conductor.analisecredito.controller;

import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.service.PessoaService;
import com.conductor.analisecredito.util.HeaderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/pessoa"})
public class PessoaController {

    @Autowired
    PessoaService pessoaService;

    @GetMapping(path = {"/{id}"})
    public ResponseEntity findById(@PathVariable Long id) throws Exception {
        return pessoaService.buscarPessoaId(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @RequestMapping(path = {"/salvar"}, method = RequestMethod.POST)
    public ResponseEntity<Void> findById(@RequestBody Pessoa pessoa) throws Exception {

        pessoaService.salvar(pessoa);

        return ResponseEntity.ok().
                headers(HeaderUtil.criarAlertaSucesso("Pessoa salva com sucesso")).build();
    }

}
