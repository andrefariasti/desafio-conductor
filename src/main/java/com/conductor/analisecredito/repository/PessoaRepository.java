package com.conductor.analisecredito.repository;

import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.entidade.Proposta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    List<Pessoa> findByNomeContaining(String nome);

    Pessoa findByCpf(String cpf);
}

