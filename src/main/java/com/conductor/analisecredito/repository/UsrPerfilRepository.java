package com.conductor.analisecredito.repository;

import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.entidade.UsrPerfil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsrPerfilRepository extends JpaRepository<UsrPerfil, Long> {
}

