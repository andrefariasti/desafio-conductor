package com.conductor.analisecredito.repository;

import com.conductor.analisecredito.entidade.Endereco;
import com.conductor.analisecredito.entidade.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Long> {
}

