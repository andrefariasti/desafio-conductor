package com.conductor.analisecredito.repository;

import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.entidade.Proposta;
import com.conductor.analisecredito.util.SituacaoPropostaEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PropostaRepository extends JpaRepository<Proposta, Long> {

    List<Proposta> findBySituacao(SituacaoPropostaEnum situacaoPropostaEnum);

    List<Proposta> findAllByPessoaIn(List<Pessoa> listaPessoa);
}
