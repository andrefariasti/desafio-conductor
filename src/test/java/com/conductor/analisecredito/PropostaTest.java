package com.conductor.analisecredito;

import com.conductor.analisecredito.entidade.Endereco;
import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.entidade.Proposta;
import com.conductor.analisecredito.entidade.Usuario;
import com.conductor.analisecredito.repository.EnderecoRepository;
import com.conductor.analisecredito.repository.PessoaRepository;
import com.conductor.analisecredito.repository.PropostaRepository;
import com.conductor.analisecredito.repository.UsuarioRepository;
import com.conductor.analisecredito.util.SituacaoPropostaEnum;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PropostaTest {

    @Autowired
    private PessoaRepository pessoaRepository;
    @Autowired
    private EnderecoRepository enderecoRepository;

    @Autowired
    private PropostaRepository propostaRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void salvarProposta(){
        Proposta proposta = novaProposta();
        Assertions.assertThat(proposta.getId()).isNotNull();
    }

    @Test
    public void rejeitarProposta(){
        Proposta proposta = novaProposta();
        proposta.setSituacao(SituacaoPropostaEnum.REJEITADA);
        proposta = propostaRepository.save(proposta);
        Assertions.assertThat(proposta.getSituacao()).isEqualTo(SituacaoPropostaEnum.REJEITADA);
    }

    @Test
    public void aprovarProposta(){
        Proposta proposta = novaProposta();
        proposta.setSituacao(SituacaoPropostaEnum.APROVADA);
        proposta = propostaRepository.save(proposta);
        Assertions.assertThat(proposta.getSituacao()).isEqualTo(SituacaoPropostaEnum.APROVADA);
    }

    @Test
    public void buscarPropostaId(){
        Proposta proposta = novaProposta();
        Optional<Proposta> propostaConsulta = propostaRepository.findById(proposta.getId());
        Assertions.assertThat(proposta.getId()).isEqualTo(proposta.getId());
    }

    @Test
    public void buscarProposta() {

        Proposta proposta = novaProposta();

        List<Pessoa> pessoaList = new ArrayList<>();
        pessoaList.add(proposta.getPessoa());
        List<Proposta> propostaConsultada1 = propostaRepository.findAllByPessoaIn(pessoaList);
        Assertions.assertThat(propostaConsultada1).isNotEmpty();
    }

    @Test
    public void buscarTodasPropostas(){
        Proposta proposta = novaProposta();
        List<Proposta> propostaList = propostaRepository.findBySituacao(SituacaoPropostaEnum.EM_ANALISE);

        if(!propostaList.isEmpty()){
            Assertions.assertThat(propostaList.get(0).getSituacao())
                    .isEqualTo(SituacaoPropostaEnum.EM_ANALISE);
        }
    }

    private Proposta novaProposta(){
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Andre Teste");
        pessoa.setCpf("07585463430");
        pessoa.setDtNascimento(new Date());

        Endereco endereco = new Endereco();
        endereco.setCidade("João pessoa");
        endereco = enderecoRepository.save(endereco);

        pessoa.setEndereco(endereco);
        pessoa.setEmail("andreFariasti@gmail.com");
        pessoa = pessoaRepository.save(pessoa);

        Usuario usuario = new Usuario();
        usuario.setPessoa(pessoa);
        usuario = usuarioRepository.save(usuario);

        Proposta proposta = new Proposta();
        proposta.setUsuarioAtendente(usuario);
        proposta.setDhProposta(new Date());
        proposta.setPessoa(pessoa);
        proposta.setSituacao(SituacaoPropostaEnum.EM_ANALISE);
        proposta = propostaRepository.save(proposta);

        return proposta;
    }
}
