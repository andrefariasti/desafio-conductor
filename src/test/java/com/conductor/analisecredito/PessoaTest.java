package com.conductor.analisecredito;

import com.conductor.analisecredito.entidade.Endereco;
import com.conductor.analisecredito.entidade.Pessoa;
import com.conductor.analisecredito.repository.EnderecoRepository;
import com.conductor.analisecredito.repository.PessoaRepository;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PessoaTest {

    @Autowired
    private PessoaRepository pessoaRepository;
    @Autowired
    private EnderecoRepository enderecoRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void salvarPessoa(){
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Andre Teste");
        pessoa.setCpf("07585463430");
        pessoa.setDtNascimento(new Date());

        Endereco endereco = new Endereco();
        endereco.setCidade("João pessoa");
        endereco = enderecoRepository.save(endereco);

        pessoa.setEndereco(endereco);
        pessoa.setEmail("andreFariasti@gmail.com");
        pessoaRepository.save(pessoa);
        Assertions.assertThat(pessoa.getId()).isNotNull();
    }

    @Test
    public void buscarPessoaId(){
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Andre Teste");
        pessoa.setCpf("07585463430");
        pessoa.setDtNascimento(new Date());

        Endereco endereco = new Endereco();
        endereco.setCidade("João pessoa");
        endereco = enderecoRepository.save(endereco);

        pessoa.setEndereco(endereco);
        pessoa.setEmail("andreFariasti@gmail.com");
        pessoaRepository.save(pessoa);
        Assertions.assertThat(pessoaRepository.findById(pessoa.getId())).isNotNull();
    }

    @Test
    public void buscarPessoasPorNome(){
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Andre Teste");
        pessoa.setCpf("07585463430");
        pessoa.setDtNascimento(new Date());

        Endereco endereco = new Endereco();
        endereco.setCidade("João pessoa");
        endereco = enderecoRepository.save(endereco);

        pessoa.setEndereco(endereco);
        pessoa.setEmail("andreFariasti@gmail.com");
        pessoaRepository.save(pessoa);
        Assertions.assertThat(pessoaRepository.findByNomeContaining("Andre Teste")).isNotEmpty();
    }

    @Test
    public void buscarPessoaPorCPF(){
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Andre Teste");
        pessoa.setCpf("07585463431");
        pessoa.setDtNascimento(new Date());

        Endereco endereco = new Endereco();
        endereco.setCidade("João pessoa");
        endereco = enderecoRepository.save(endereco);

        pessoa.setEndereco(endereco);
        pessoa.setEmail("andreFariasti@gmail.com");
        pessoaRepository.save(pessoa);
        Assertions.assertThat(pessoaRepository.findByCpf("07585463431")).isEqualTo(pessoa);
    }
}
