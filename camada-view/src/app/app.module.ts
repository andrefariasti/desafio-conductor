import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http'
import { TextMaskModule } from 'angular2-text-mask';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadastrarPropostaComponent } from './features/proposta/cadastrar-proposta/cadastrar-proposta.component';
import { FooterComponent } from './template/footer/footer.component';
import { HeaderComponent } from './template/header/header.component';
import { LoginComponent } from './template/login/login.component';
import { AnalisarPropostaComponent } from './features/proposta/analisar-proposta/analisar-proposta.component';
import { ConsultarPropostaComponent } from './features/proposta/consultar-proposta/consultar-proposta.component';
import { AnalisarTodasPropostasComponent } from './features/proposta/analisar-todas-propostas/analisar-todas-propostas.component';
import { VerificarPermissaoLogadoGuard, VerificarUsuarioLogadoGuard } from './guards/verificar-usuario-logado.guard';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent,
    CadastrarPropostaComponent,
    AnalisarPropostaComponent,
    ConsultarPropostaComponent,
    AnalisarTodasPropostasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CommonModule,
    TextMaskModule
  ],
  providers: [
    VerificarUsuarioLogadoGuard,
    VerificarPermissaoLogadoGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
