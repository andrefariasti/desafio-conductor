import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalisarPropostaComponent } from './analisar-proposta.component';

describe('AnalisarPropostaComponent', () => {
  let component: AnalisarPropostaComponent;
  let fixture: ComponentFixture<AnalisarPropostaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalisarPropostaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalisarPropostaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
