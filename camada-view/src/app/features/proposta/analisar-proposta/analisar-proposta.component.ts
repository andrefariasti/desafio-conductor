import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Mask } from 'src/app/mask/mask.util'

@Component({
  selector: 'app-analisar-proposta',
  templateUrl: './analisar-proposta.component.html',
  styleUrls: ['./analisar-proposta.component.css']
})
export class AnalisarPropostaComponent implements OnInit {

  public baseUrl = environment.api_base_url
  public url = `${this.baseUrl}/proposta`;

  get maskUtil() { return Mask }
  
  public nome: any;
  public cpf: any;
  public dtNascimento: any;
  public telefone: any;
  public email: any;
  public estado: any;
  public cidade: any;
  public bairro: any;
  public rua: any;
  public numero: any;
  public complemento: any;

  constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private route: Router) { }

  ngOnInit() {
    this.http.get(`${this.url}/${this.activatedRoute.snapshot.params['id']}`).subscribe((retorno: any) => {
      this.nome = retorno.pessoa.nome
      this.cpf = retorno.pessoa.cpf
      this.dtNascimento = retorno.pessoa.dtNascimento
      this.telefone = retorno.pessoa.telefone
      this.email = retorno.pessoa.email
      this.estado = retorno.pessoa.endereco.estado
      this.cidade = retorno.pessoa.endereco.cidade
      this.bairro = retorno.pessoa.endereco.bairro
      this.rua = retorno.pessoa.endereco.rua
      this.numero = retorno.pessoa.endereco.numero
      this.complemento = retorno.pessoa.endereco.complemento
    })
  }

  aprovarProposta() {
    this.http.get(`${this.url}/aprovar/${this.activatedRoute.snapshot.params['id']}`).subscribe((retorno: any) => {
     alert('Operação realizada com sucesso!')
     this.route.navigate(['/proposta/analisar']);
    })
  }

  rejeitarProposta() {
    this.http.get(`${this.url}/rejeitar/${this.activatedRoute.snapshot.params['id']}`).subscribe((retorno: any) => {
      alert('Operação realizada com sucesso!')
      this.route.navigate(['/proposta/analisar']);
    })
  }

}
