import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Mask } from 'src/app/mask/mask.util'

@Component({
  selector: 'app-consultar-proposta',
  templateUrl: './consultar-proposta.component.html',
  styleUrls: ['./consultar-proposta.component.css']
})
export class ConsultarPropostaComponent implements OnInit {

  public listaPropostas: any = []

  public baseUrl = environment.api_base_url
  public url = `${this.baseUrl}/proposta/buscar`;

  get maskUtil() { return Mask }

  public nome: any;
  public cpf: any;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() { }

  consultarPropostas() {
    const params = new URLSearchParams()
    this.nome && params.append('nome', this.nome)
    this.cpf && params.append('cpf', this.cpf)
    this.http.get(`${this.baseUrl}/proposta/buscar-por-filtro?${params.toString()}`).subscribe(proposta => {
      this.listaPropostas = proposta
    })
  }

  analisarProposta(idProposta: any) {
    this.router.navigate([`/proposta/analisar/${idProposta}`])
  }

}
