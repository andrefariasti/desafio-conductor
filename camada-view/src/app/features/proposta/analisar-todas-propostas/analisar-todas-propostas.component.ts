import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Mask } from 'src/app/mask/mask.util'

@Component({
  selector: 'app-analisar-todas-propostas',
  templateUrl: './analisar-todas-propostas.component.html',
  styleUrls: ['./analisar-todas-propostas.component.css']
})
export class AnalisarTodasPropostasComponent implements OnInit {

  public listaPropostas: any = []

  public baseUrl = environment.api_base_url
  public url = `${this.baseUrl}/proposta/buscar`;

  get maskUtil() { return Mask }

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.http.get(`${this.url}`).subscribe(proposta => {
      this.listaPropostas = proposta
    })
  }

  analisarProposta(idProposta: any) {
    this.router.navigate([`/proposta/analisar/${idProposta}`])
  }

}
