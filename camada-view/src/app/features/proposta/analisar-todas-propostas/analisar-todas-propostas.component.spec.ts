import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalisarTodasPropostasComponent } from './analisar-todas-propostas.component';

describe('AnalisarTodasPropostasComponent', () => {
  let component: AnalisarTodasPropostasComponent;
  let fixture: ComponentFixture<AnalisarTodasPropostasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalisarTodasPropostasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalisarTodasPropostasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
