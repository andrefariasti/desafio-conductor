import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnalisarPropostaComponent } from './features/proposta/analisar-proposta/analisar-proposta.component';
import { AnalisarTodasPropostasComponent } from './features/proposta/analisar-todas-propostas/analisar-todas-propostas.component';
import { CadastrarPropostaComponent } from './features/proposta/cadastrar-proposta/cadastrar-proposta.component';
import { ConsultarPropostaComponent } from './features/proposta/consultar-proposta/consultar-proposta.component';
import { LoginComponent } from './template/login/login.component';
import { VerificarPermissaoLogadoGuard, VerificarUsuarioLogadoGuard } from 'src/app/guards/verificar-usuario-logado.guard'

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: '',
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: 'proposta/cadastrar',
    component: CadastrarPropostaComponent,
    pathMatch: 'full',
    canActivate: [VerificarUsuarioLogadoGuard, VerificarPermissaoLogadoGuard],
    data: { perfil: '3' }
  },
  {
    path: 'proposta/analisar/:id',
    component: AnalisarPropostaComponent,
    pathMatch: 'full',
    canActivate: [VerificarUsuarioLogadoGuard, VerificarPermissaoLogadoGuard],
    data: { perfil: '2' }
  },
  {
    path: 'proposta/analisar',
    component: AnalisarTodasPropostasComponent,
    pathMatch: 'full',
    canActivate: [VerificarUsuarioLogadoGuard, VerificarPermissaoLogadoGuard],
    data: { perfil: '2' }
  },
  {
    path: 'proposta/consultar',
    component: ConsultarPropostaComponent,
    pathMatch: 'full',
    canActivate: [VerificarUsuarioLogadoGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
